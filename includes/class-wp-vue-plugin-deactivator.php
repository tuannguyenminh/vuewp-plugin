<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.linkedin.com/in/minhtuan2086/
 * @since      1.0.0
 *
 * @package    Wp_Vue_Plugin
 * @subpackage Wp_Vue_Plugin/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wp_Vue_Plugin
 * @subpackage Wp_Vue_Plugin/includes
 * @author     TUAN Nguyen minh <minhtuan2086@gmail.com>
 */
class Wp_Vue_Plugin_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
